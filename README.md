Okay, clone it like this
```
cd ~/Library/Keyboard\ Layouts
git clone https://veseloff@bitbucket.org/veseloff/typographic-os-x-layout.git EnRuUaTypographic.bundle
```
If you want to leave only these layouts, you should remove all others.
First of all convert to xml.
```
plutil -convert xml1 ~/Library/Preferences/com.apple.HIToolbox.plist
```
And edit converted file. 